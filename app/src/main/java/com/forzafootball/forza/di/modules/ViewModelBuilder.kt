package com.forzafootball.forza.di.modules

import android.arch.lifecycle.ViewModelProvider
import com.forzafootball.forza.di.CViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelBuilder {

  @Binds
  abstract fun bindViewModelFactory(factory: CViewModelFactory): ViewModelProvider.Factory

}