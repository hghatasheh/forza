package com.forzafootball.forza.di.modules

import android.content.Context
import com.forzafootball.forza.R
import com.forzafootball.forza.common.rx.DefaultRxSchedulers
import com.forzafootball.forza.common.rx.RxSchedulers
import com.forzafootball.forza.network.AppApi
import com.forzafootball.forza.network.RestClient
import com.github.simonpercic.oklog3.OkLogInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

  @Singleton
  @Provides
  fun restClient(retrofit: Retrofit): RestClient {
    return RestClient(retrofit)
  }

  @Singleton
  @Provides
  fun api(restClient: RestClient): AppApi = restClient.appApi

  @Singleton
  @Provides
  fun retrofit(context: Context, okHttpClient: OkHttpClient, gson: Gson): Retrofit {
    return Retrofit.Builder()
      .baseUrl(context.getString(R.string.baseUrl))
      .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
      .addConverterFactory(GsonConverterFactory.create(gson))
      .client(okHttpClient)
      .build()
  }

  @Singleton
  @Provides
  fun okHttpClient(logInterceptor: OkLogInterceptor): OkHttpClient {
    return OkHttpClient.Builder()
      .addNetworkInterceptor(logInterceptor)
      .build()
  }

  @Singleton
  @Provides
  fun gson(): Gson {
    return GsonBuilder().create()
  }

  @Singleton
  @Provides
  fun okLogInterceptor(): OkLogInterceptor {
    return OkLogInterceptor.builder()
      .setBaseUrl("http://localhost:8080")
      .withRequestHeaders(true)
      .withResponseHeaders(true)
      .withRequestUrl(true)
      .withAllLogData()
      .build()
  }

  @Singleton
  @Provides
  fun rxSchedulers(): RxSchedulers = DefaultRxSchedulers()
}