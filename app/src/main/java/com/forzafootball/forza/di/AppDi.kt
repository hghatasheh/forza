package com.forzafootball.forza.di

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.forzafootball.forza.ForzaApp
import com.forzafootball.forza.dashboard.DashboardActivityBuilder
import com.forzafootball.forza.di.modules.NetworkModule
import com.forzafootball.forza.di.modules.ViewModelBuilder
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
  AndroidSupportInjectionModule::class,
  AppModule::class,
  NetworkModule::class,
  ViewModelBuilder::class,
  DashboardActivityBuilder::class
])
interface AppComponent : AndroidInjector<ForzaApp> {

  @Component.Builder
  abstract class Builder : AndroidInjector.Builder<ForzaApp>()
}

@Module
class AppModule {

  @Provides
  fun context(forzaApp: ForzaApp): Context = forzaApp.applicationContext

  @Singleton
  @Provides
  fun preferences(app: ForzaApp): SharedPreferences {
    return PreferenceManager.getDefaultSharedPreferences(app)
  }
}