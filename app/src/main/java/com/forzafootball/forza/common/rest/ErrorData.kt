package com.forzafootball.forza.common.rest

data class ErrorResponse(val error: String)