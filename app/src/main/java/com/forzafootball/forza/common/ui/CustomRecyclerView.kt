package com.forzafootball.forza.common.ui

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.forzafootball.forza.R
import com.forzafootball.forza.common.ext.getString
import kotlinx.android.synthetic.main.custom_list.view.infoView
import kotlinx.android.synthetic.main.custom_list.view.recyclerView

class CustomRecyclerView @JvmOverloads constructor(
  context: Context,
  attrs: AttributeSet? = null,
  defStyleAttr: Int = 0) : FrameLayout(context, attrs, defStyleAttr) {

  init {
    View.inflate(context, R.layout.custom_list, this@CustomRecyclerView)
  }

  fun showList() {
    toggleVisibility(View.GONE, View.VISIBLE)
  }

  fun showLoading(loading: Boolean) {
    toggleVisibility(View.VISIBLE, View.GONE)
    infoView.progress(loading)
  }

  fun showNoInternet() {
    toggleVisibility(View.VISIBLE, View.GONE)
    customiseAnimationView(
      messageText = getString(R.string.error_no_internet_connection),
      fileName = "animation_no_internet.json")
  }

  fun showEmpty() {
    toggleVisibility(View.VISIBLE, View.GONE)
    customiseAnimationView(
      messageText = getString(R.string.error_general),
      fileName = "animation_empty.json")
  }

  private fun customiseAnimationView(titleText: String? = null, messageText: String? = null, fileName: String) {
    infoView.apply {
      iconAnimation(fileName)
      titleText?.let { title(it) }
      messageText?.let { message(it) }
      buttonText(R.string.common_try_again)
      progress(false)
    }
  }

  private fun toggleVisibility(info: Int, list: Int) {
    infoView.visibility = info
    recyclerView.visibility = list
  }
}