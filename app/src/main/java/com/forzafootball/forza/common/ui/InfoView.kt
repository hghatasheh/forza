package com.forzafootball.forza.common.ui

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.airbnb.lottie.LottieDrawable
import com.forzafootball.forza.R
import com.forzafootball.forza.common.ext.getString
import kotlinx.android.synthetic.main.info_view.view.animationView
import kotlinx.android.synthetic.main.info_view.view.infoContainer
import kotlinx.android.synthetic.main.info_view.view.messageTv
import kotlinx.android.synthetic.main.info_view.view.progressBar
import kotlinx.android.synthetic.main.info_view.view.titleTv
import kotlinx.android.synthetic.main.info_view.view.tryAgainBtn

class InfoView @JvmOverloads constructor(
  context: Context,
  attrs: AttributeSet? = null,
  defStyleAttr: Int = 0) : FrameLayout(context, attrs, defStyleAttr) {

  init {
    View.inflate(context, R.layout.info_view, this@InfoView)

    attrs?.let {
      val title: String?
      val message: String?
      val buttonText: String?
      val icon: Drawable?
      val buttonTextColor: Int

      val a = context.obtainStyledAttributes(attrs, R.styleable.InfoView, defStyleAttr, 0)

      try {
        title = a.getString(R.styleable.InfoView_info_title)
        message = a.getString(R.styleable.InfoView_info_message)
        icon = a.getDrawable(R.styleable.InfoView_info_icon)
        buttonText = a.getString(R.styleable.InfoView_info_buttonText)
        buttonTextColor = a.getColor(R.styleable.InfoView_info_buttonTextColor, 0)
      } finally {
        a.recycle()
      }

      titleTv.text = title
      messageTv.text = message
      icon?.let { animationView.setImageDrawable(it) }

      tryAgainBtn.text = buttonText

      if (buttonTextColor != 0) {
        tryAgainBtn.setTextColor(buttonTextColor)
      }
    }
  }

  fun title(title: String) {
    titleTv.text = title
  }

  fun titleRes(@StringRes titleRes: Int) {
    titleTv.text = getString(titleRes)
  }

  fun message(message: String) {
    messageTv.text = message
  }

  fun message(@StringRes messageRes: Int) {
    messageTv.text = getString(messageRes)
  }

  fun iconDrawable(iconDrawable: Drawable) {
    animationView.setImageDrawable(iconDrawable)
  }

  fun iconAnimation(name: String) {
    animationView.apply {
      setAnimation(name)
      repeatCount = LottieDrawable.INFINITE
      playAnimation()
    }
  }

  fun iconRes(@DrawableRes iconRes: Int) {
    animationView.setImageResource(iconRes)
  }

  fun buttonText(buttonText: String) {
    this.tryAgainBtn.text = buttonText
  }

  fun buttonText(@StringRes buttonTextRes: Int) {
    tryAgainBtn.text = getString(buttonTextRes)
  }

  fun buttonTextColor(@ColorInt textColor: Int) {
    this.tryAgainBtn.setTextColor(textColor)
  }

  fun buttonTextColorRes(@ColorRes textColor: Int) {
    tryAgainBtn.setTextColor(ContextCompat.getColor(context, textColor))
  }

  fun progress(setProgress: Boolean) {
    if (setProgress) {
      infoContainer.visibility = View.INVISIBLE
      progressBar.visibility = View.VISIBLE
    } else {
      infoContainer.visibility = View.VISIBLE
      progressBar.visibility = View.GONE
    }
  }

  fun setOnTryAgainListener(onTryAgainClickListener: () -> Unit) {
    tryAgainBtn.setOnClickListener {
      onTryAgainClickListener()
    }
  }
}