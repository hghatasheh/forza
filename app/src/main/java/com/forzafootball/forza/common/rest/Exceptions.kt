package com.forzafootball.forza.common.rest

sealed class CException constructor(msg: String? = null): Exception(msg)

class GeneralException constructor(msg: String? = null): CException(msg)

class NetworkException constructor(msg: String? = null): CException(msg)