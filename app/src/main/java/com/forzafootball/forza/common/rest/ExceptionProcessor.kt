package com.forzafootball.forza.common.rest

import retrofit2.HttpException
import java.io.IOException

class ExceptionProcessor(private val exceptionHandler: ExceptionHandler) {

  fun process(t: Throwable) {
    when (t) {
      is HttpException -> exceptionHandler.handleApiException(t)
      is IOException -> exceptionHandler.handleNoInternet(NetworkException())
      else -> exceptionHandler.handleGeneralException(GeneralException())
    }
  }
}