package com.forzafootball.forza.common.rest

import retrofit2.HttpException

interface ExceptionHandler {

  fun handleNoInternet(e: NetworkException)

  fun handleGeneralException(e: GeneralException)

  fun handleApiException(e: HttpException)
}