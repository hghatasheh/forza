package com.forzafootball.forza.common.rest

import android.app.Activity
import android.util.Log
import android.widget.Toast
import com.forzafootball.forza.R
import com.forzafootball.forza.common.ext.showDialog
import com.forzafootball.forza.common.ext.showToast
import com.forzafootball.forza.common.ui.SafeToast
import com.google.gson.Gson
import retrofit2.HttpException

open class DefaultExceptionHandler(
  protected val activity: Activity,
  protected val gson: Gson) : ExceptionHandler {

  override fun handleNoInternet(e: NetworkException) {
    SafeToast.makeText(activity, R.string.error_no_internet_connection, Toast.LENGTH_SHORT).show()
  }

  override fun handleGeneralException(e: GeneralException) {
    SafeToast.makeText(activity, R.string.error_general, Toast.LENGTH_SHORT).show()
  }

  override fun handleApiException(e: HttpException) {
    val string = e.response()?.errorBody()?.string()
    if (!string.isNullOrEmpty()) {
      try {
        val errorBody: ErrorResponse = gson.fromJson(string, ErrorResponse::class.java)

        val showDialog = showDialog(
          context = activity,
          message = errorBody.error)
        if (!activity.isFinishing) {
          showDialog.show()
        }
      } catch (e: Exception) {
        Log.e("ExceptionHandler", e.message, e)
        showToast(activity)
      }
    }
  }
}