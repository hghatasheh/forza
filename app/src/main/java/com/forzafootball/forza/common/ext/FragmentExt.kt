package com.forzafootball.forza.common.ext

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction

inline fun FragmentManager.inTransaction(backStack: String?,
  f: FragmentTransaction.() -> Unit) {
  val transaction = beginTransaction()
  transaction.f()
  if (backStack != null) {
    transaction.addToBackStack(backStack)
  }
  transaction.commit()
}

fun FragmentActivity.replaceFragment(fragment: Fragment, frameId: Int,
  addToBackStack: String? = null) {
  supportFragmentManager.inTransaction(addToBackStack) { replace(frameId, fragment) }
}

fun Fragment.replaceFragment(fragment: Fragment, frameId: Int,
  addToBackStack: String? = null) {
  activity?.supportFragmentManager?.inTransaction(addToBackStack) {
    replace(frameId, fragment)
  }
}