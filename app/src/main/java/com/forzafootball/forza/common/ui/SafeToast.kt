package com.forzafootball.forza.common.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.os.Handler
import android.os.Looper
import android.widget.Toast

class SafeToast(context: Context) : Toast(context) {

  override fun show() {
    if (Looper.myLooper() == Looper.getMainLooper()) {
      super.show()
    } else {
      val handler = Handler(Looper.getMainLooper())
      handler.post { super@SafeToast.show() }
    }
  }

  companion object {

    fun makeText(context: Context, text: CharSequence, duration: Int): Toast {
      @SuppressLint("ShowToast")
      val origToast = Toast.makeText(context, text, duration)
      val safeToast = SafeToast(context)
      safeToast.view = origToast.view
      safeToast.duration = origToast.duration
      return safeToast
    }

    @Throws(Resources.NotFoundException::class)
    fun makeText(context: Context, resId: Int, duration: Int): Toast {
      return makeText(context, context.resources.getText(resId), duration)
    }
  }
}