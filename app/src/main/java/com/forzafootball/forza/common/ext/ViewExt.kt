package com.forzafootball.forza.common.ext

import android.support.annotation.StringRes
import android.view.View

fun View.getString(@StringRes resId: Int): String {
  return context.resources.getString(resId)
}