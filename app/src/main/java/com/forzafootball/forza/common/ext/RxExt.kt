package com.forzafootball.forza.common.ext

import com.forzafootball.forza.common.rx.RxSchedulers
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

operator fun CompositeDisposable.plusAssign(disposable: Disposable) {
  add(disposable)
}

fun <T> Single<T>.networkToMain(rxSchedulers: RxSchedulers): Single<T> {
  return subscribeOn(rxSchedulers.network())
    .observeOn(rxSchedulers.main())
}