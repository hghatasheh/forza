package com.forzafootball.forza.network

import com.forzafootball.forza.dashboard.data.TeamResponse
import io.reactivex.Single
import retrofit2.http.GET

interface AppApi {

  @GET("android/teams.json")
  fun teams(): Single<List<TeamResponse>>
}