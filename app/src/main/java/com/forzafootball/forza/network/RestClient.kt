package com.forzafootball.forza.network

import retrofit2.Retrofit

class RestClient(private val retrofit: Retrofit) {

  private fun <T> getApi(api: Class<T>): T = retrofit.create(api)

  val appApi: AppApi by lazy { getApi(AppApi::class.java) }
}