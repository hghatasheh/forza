package com.forzafootball.forza

import android.arch.lifecycle.LifecycleRegistry
import android.os.Bundle
import android.view.MenuItem
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity() {
  private var lifecycleRegistry: LifecycleRegistry? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    lifecycleRegistry = LifecycleRegistry(this)
  }

  override fun onStart() {
    super.onStart()
    supportActionBar?.apply {
      setDisplayHomeAsUpEnabled(enableBackButton())
    }
  }

  open fun enableBackButton(): Boolean = false

  override fun getLifecycle(): LifecycleRegistry {
    return lifecycleRegistry!!
  }

  override fun onDestroy() {
    super.onDestroy()
    lifecycleRegistry = null
  }

  override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    return when (item?.itemId) {
      android.R.id.home -> {
        onBackPressed()
        return true
      }
      else -> super.onOptionsItemSelected(item)
    }
  }
}