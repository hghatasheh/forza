package com.forzafootball.forza.dashboard.teams.views

import android.content.Context
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.forzafootball.forza.R
import com.forzafootball.forza.dashboard.data.TeamResponse
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Section
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.custom_list.view.infoView
import kotlinx.android.synthetic.main.custom_list.view.recyclerView
import kotlinx.android.synthetic.main.teams_fragment.view.customRv

class TeamsView @JvmOverloads constructor(
  context: Context,
  attrs: AttributeSet? = null,
  defStyleAttr: Int = 0) : FrameLayout(context, attrs, defStyleAttr) {

  private val groupAdapter = GroupAdapter<ViewHolder>()
  private val section = Section()

  init {
    View.inflate(context, R.layout.teams_fragment, this)

    customRv.recyclerView.apply {
      layoutManager = LinearLayoutManager(context)
      addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
      adapter = groupAdapter
    }
  }

  fun updateAdapter(list: List<TeamResponse>) {
    if (list.size != groupAdapter.itemCount) {
      section.update(list.map { TeamItemView(it) })
    }
    groupAdapter.add(section)
    customRv.showList()
  }

  fun showLoading(show: Boolean) {
    customRv.showLoading(show)
  }

  fun setRetry(listener: () -> Unit) {
    customRv.infoView.setOnTryAgainListener {
      listener()
    }
  }
}