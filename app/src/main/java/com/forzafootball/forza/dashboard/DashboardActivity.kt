package com.forzafootball.forza.dashboard

import android.os.Bundle
import com.forzafootball.forza.BaseActivity
import com.forzafootball.forza.R
import com.forzafootball.forza.common.ext.replaceFragment
import com.forzafootball.forza.dashboard.teams.TeamsFragment
import kotlinx.android.synthetic.main.dashboard_activity.toolbar

class DashboardActivity : BaseActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.dashboard_activity)
    setSupportActionBar(toolbar)

    if (savedInstanceState == null) {
      replaceFragment(TeamsFragment.newInstance(), R.id.container)
    }

  }
}
