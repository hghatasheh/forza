package com.forzafootball.forza.dashboard.teams

import android.arch.lifecycle.ViewModel
import com.forzafootball.forza.common.rest.ExceptionProcessor
import com.forzafootball.forza.dashboard.teams.views.TeamsView
import com.forzafootball.forza.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class TeamsFragmentBuilder {

  @ContributesAndroidInjector(modules = [
    TeamsFragmentModule::class
  ])
  internal abstract fun teamsFragment(): TeamsFragment

  @Binds
  @IntoMap
  @ViewModelKey(TeamsViewModel::class)
  internal abstract fun bindTeamsFragmentViewModel(viewModel: TeamsViewModel): ViewModel
}

@Module
class TeamsFragmentModule {

  @Provides
  fun view(fragment: TeamsFragment): TeamsView {
    return TeamsView(fragment.context!!)
  }

  @Provides
  fun exceptionProcessor(handler: TeamsExceptionHandler): ExceptionProcessor = ExceptionProcessor(handler)
}