package com.forzafootball.forza.dashboard

import com.forzafootball.forza.dashboard.teams.TeamsFragmentBuilder
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class DashboardActivityBuilder {

  @ContributesAndroidInjector(modules = [
    TeamsFragmentBuilder::class
  ])
  internal abstract fun dashboardActivity(): DashboardActivity
}