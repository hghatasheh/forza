package com.forzafootball.forza.dashboard.data

import com.google.gson.annotations.SerializedName

data class TeamResponse(
  @SerializedName("name") val name: String,
  @SerializedName("national") val national: Boolean,
  @SerializedName("country_name") val countryName: String
)