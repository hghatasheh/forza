package com.forzafootball.forza.dashboard.teams

import android.arch.lifecycle.ViewModelProvider.Factory
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.forzafootball.forza.BaseFragment
import com.forzafootball.forza.common.Failure
import com.forzafootball.forza.common.Loading
import com.forzafootball.forza.common.Success
import com.forzafootball.forza.common.ext.observeK
import com.forzafootball.forza.common.rest.ExceptionProcessor
import com.forzafootball.forza.dashboard.teams.views.TeamsView
import kotlinx.android.synthetic.main.teams_fragment.view.customRv
import javax.inject.Inject

class TeamsFragment : BaseFragment() {

  @Inject lateinit var viewModelFactory: Factory
  private lateinit var viewModel: TeamsViewModel
  @Inject lateinit var exceptionProcessor: ExceptionProcessor
  @Inject lateinit var teamsView: TeamsView

  companion object {
    fun newInstance(): TeamsFragment {
      return TeamsFragment()
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    viewModel = ViewModelProviders.of(this, viewModelFactory).get(TeamsViewModel::class.java)
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    return teamsView
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    teamsView.setRetry {
      viewModel.loadTeams()
    }
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)

    viewModel.data.observeK(this) {
      when (it?.status) {
        Loading -> {
          teamsView.showLoading(true)
        }
        Success -> {
          teamsView.updateAdapter(it.data ?: emptyList())
        }
        Failure -> {
          teamsView.customRv.showEmpty()
//          exceptionProcessor.process(it.throwable ?: GeneralException())
        }
      }
    }
  }

}