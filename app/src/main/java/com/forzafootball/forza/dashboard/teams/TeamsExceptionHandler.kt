package com.forzafootball.forza.dashboard.teams

import com.forzafootball.forza.common.rest.DefaultExceptionHandler
import com.forzafootball.forza.common.rest.ExceptionHandler
import com.forzafootball.forza.common.rest.GeneralException
import com.forzafootball.forza.common.rest.NetworkException
import com.forzafootball.forza.dashboard.DashboardActivity
import com.forzafootball.forza.dashboard.teams.views.TeamsView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.teams_fragment.view.customRv
import javax.inject.Inject

class TeamsExceptionHandler @Inject constructor(
  activity: DashboardActivity,
  private val teamsView: TeamsView,
  gson: Gson) : ExceptionHandler by DefaultExceptionHandler(activity, gson) {

  override fun handleNoInternet(e: NetworkException) {
    teamsView.customRv.showNoInternet()
  }

  override fun handleGeneralException(e: GeneralException) {
    teamsView.customRv.showEmpty()
  }
}