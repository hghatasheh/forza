package com.forzafootball.forza.dashboard.teams

import android.arch.lifecycle.MutableLiveData
import com.forzafootball.forza.common.Resource
import com.forzafootball.forza.common.ext.plusAssign
import com.forzafootball.forza.common.rx.RxAwareViewModel
import com.forzafootball.forza.common.rx.RxSchedulers
import com.forzafootball.forza.dashboard.data.TeamResponse
import javax.inject.Inject

class TeamsViewModel @Inject constructor(
  private val teamsRepository: TeamsRepository,
  private val rxSchedulers: RxSchedulers) : RxAwareViewModel() {

  val data: MutableLiveData<Resource<List<TeamResponse>>> = MutableLiveData()

  init {
    loadTeams()
  }

  fun loadTeams() {
    disposables += teamsRepository.teams()
      .doOnSubscribe { data.postValue(Resource.loading()) }
      .subscribeOn(rxSchedulers.network())
      .observeOn(rxSchedulers.main())
      .subscribe(
        { data.value = Resource.success(it) },
        { data.value = Resource.error(it) }
      )
  }
}