package com.forzafootball.forza.dashboard.teams.views

import com.forzafootball.forza.R
import com.forzafootball.forza.dashboard.data.TeamResponse
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.teams_item_view.view.teamCountryTv
import kotlinx.android.synthetic.main.teams_item_view.view.teamNameTv

class TeamItemView(private val team: TeamResponse) : Item<ViewHolder>() {
  override fun getLayout(): Int = R.layout.teams_item_view

  override fun bind(viewHolder: ViewHolder, position: Int) {
    viewHolder.itemView.apply {
      teamNameTv.text = team.name
      teamCountryTv.text = team.countryName
    }
  }
}