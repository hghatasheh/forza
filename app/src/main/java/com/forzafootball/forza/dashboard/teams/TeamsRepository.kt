package com.forzafootball.forza.dashboard.teams

import com.forzafootball.forza.dashboard.data.TeamResponse
import com.forzafootball.forza.network.AppApi
import io.reactivex.Single
import javax.inject.Inject

class TeamsRepository @Inject constructor(private val api: AppApi) {

  fun teams(): Single<List<TeamResponse>> {
    return api.teams()
  }
}