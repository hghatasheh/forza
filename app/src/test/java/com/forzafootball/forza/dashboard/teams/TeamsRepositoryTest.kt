package com.forzafootball.forza.dashboard.teams

import com.forzafootball.forza.dashboard.data.TeamResponse
import com.forzafootball.forza.getMockAs
import com.forzafootball.forza.network.AppApi
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import java.io.IOException

class TeamsRepositoryTest {

  private lateinit var api: AppApi
  private lateinit var repository: TeamsRepository

  @Before
  fun setUp() {
    api = mock()
    repository = TeamsRepository(api)
  }

  @Test
  fun teams() {
    val teamsMock = getMockAs<List<TeamResponse>>("teams.json")
    whenever(api.teams()).thenReturn(Single.just(teamsMock))
    val single = repository.teams()

    single.test()
      .assertNoErrors()
      .assertComplete()
      .assertResult(teamsMock)
  }

  @Test
  fun testTeamsNoInternet() {
    val ioException = IOException()
    whenever(api.teams()).thenReturn(Single.error(ioException))
    val single = repository.teams()

    single.test()
      .assertError(ioException)
  }
}