package com.forzafootball.forza

import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import java.io.FileReader

inline fun <reified T> getMockAs(fileName: String): T? {
  val gson = Gson()

  try {
    val url = ClassLoader.getSystemResource("mocks/" + fileName)
    val jsonReader = JsonReader(FileReader(url.file))
    return gson.fromJson(jsonReader, T::class.java)
  } catch (e: Exception) {
    e.printStackTrace()
  }

  return null
}